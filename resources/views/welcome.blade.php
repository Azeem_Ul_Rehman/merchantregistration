<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <title>Merchant Registration</title>
    <!-- Required meta tags always come first -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <!-- roboto fonts -->
    <link href="https://fonts.googleapis.com/css?family=Arvo:400,700" rel="stylesheet">
    <!-- Bootstrap CSS -->
    <!-- <link rel="stylesheet" href="node_modules/bootstrap/dist/css/bootstrap.min.css"> -->

    <!-- animate.css -->
    <!-- <link rel="stylesheet" href="animate.css-master/animate.css"> -->
    <!-- slick slider css-->
    <link rel="stylesheet" href="{{ asset('./slick-slider/css/slick.css') }}">
    <!-- Custom CSS -->
    <link rel="stylesheet" href="{{ asset('./css/styles.css') }}">

    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
          integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

    <!-- Optional theme -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css"
          integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

    <link href="https://fonts.googleapis.com/css?family=Poppins:400,500,600" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/AlertifyJS/1.11.2/css/alertify.min.css"/>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/AlertifyJS/1.11.2/alertify.min.js"></script>


</head>

<body class="bg">

<div class="container">
    <div class="logo">
        <img src="{{ asset('./img/logo.png') }}"/>
    </div>
    <div class="">
        <div class="">
            <h1 class="font-h">MERCHANT INFORMATION</h1>
            <P class="font-p">Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an
                unknown
                printer took a galley of type and scrambled it<br>
                to make a type specimen book.</P>
        </div>
        <div class="padding-tab">

            <ul class="nav nav-tabs m-t-20">
                <li class="active "><a data-toggle="tab" class="home" href="#home">1. Personal Information</a></li>
                <li><a data-toggle="tab" href="#menu1" class="menu">2. Company Information</a></li>

            </ul>

            <div class="tab-content bg-white">
                <form method="post" enctype="multipart/form-data" action="{{ route('merchant-registration') }}">
                    @csrf
                    <div id="home" class="tab-pane fade in active">

                        <div class="m-p-0 col-md-12 col-lg-12 col-xs-12 col-sm-12">
                            <div class="col-md-5 col-xs-12 col-sm-12">
                                <div class="form-group">
                                    <label for="name" class="exampleInputEmail1">*Your Name</label>
                                    <input type="text" class="input-field" id="name"
                                           aria-describedby="emailHelp" name="name" required
                                           placeholder="Enter your name">
                                </div>
                            </div>
                            <div class="col-md-5 col-xs-12 col-sm-12">
                                <div class="form-group">
                                    <label for="email" class="exampleInputEmail1">*Your Email
                                        Address</label>
                                    <input type="email" class="input-field" id="email"
                                           aria-describedby="emailHelp" name="email" required
                                           placeholder="Enter your email address">
                                </div>
                            </div>
                            <div class="col-md-1 col-lg-1">
                            </div>
                            <div class="col-md-1 col-lg-1">
                            </div>
                        </div>

                        <div class="text-right p-r-10 ">
                            <a class="btn-sub cont" href="#">Next</a>
                        </div>


                    </div>
                    <div id="menu1" class="tab-pane fade ">

                        <div class="m-p-0 col-md-12 col-lg-12 col-xs-12 col-sm-12">
                            <div class="col-md-5 col-xs-12 col-sm-12">
                                <div class="form-group">
                                    <label for="exampleInputEmail1" class="exampleInputEmail1">*Your Company
                                        Name</label>
                                    <input type="text" class="input-field" id="exampleInputEmail1"
                                           aria-describedby="emailHelp" name="company_name" required
                                           placeholder="Enter your company name">
                                </div>
                            </div>
                            <div class="col-md-5 col-xs-12 col-sm-12">
                                <div class="form-group">
                                    <label for="exampleInputEmail1" class="exampleInputEmail1">*Your Business
                                        Name</label>
                                    <input type="text" class="input-field" id="exampleInputEmail1"
                                           aria-describedby="emailHelp" name="business_name" required
                                           placeholder="Enter your business name">
                                </div>
                            </div>
                            <div class="col-md-1 col-lg-1">
                            </div>
                            <div class="col-md-1 col-lg-1">
                            </div>
                        </div>
                        <div class="m-p-0 col-md-12 col-lg-12 col-xs-12 col-sm-12">
                            <div class="col-md-5 col-xs-12 col-sm-12">
                                <div class="form-group">
                                    <label for="exampleInputEmail1" class="exampleInputEmail1">Your Company
                                        Registration</label>
                                    <input type="text" class="input-field" id="exampleInputEmail1"
                                           aria-describedby="emailHelp" name="company_registration"
                                           placeholder="Enter Company Registration">
                                </div>
                            </div>
                            <div class="col-md-5 col-xs-12 col-sm-12">
                                <div class="form-group">
                                    <label for="exampleInputEmail1" class="exampleInputEmail1">Contact
                                        Information</label>
                                    <input type="text" class="input-field" id="exampleInputEmail1"
                                           aria-describedby="emailHelp" name="contact_information"
                                           placeholder="Enter your contact information">
                                </div>
                            </div>
                            <div class="col-md-1 col-lg-1">
                            </div>
                            <div class="col-md-1 col-lg-1">
                            </div>
                        </div>
                        <div class="m-p-0 col-md-12 col-lg-12 col-xs-12 col-sm-12">
                            <div class="col-md-5 col-xs-12 col-sm-12">
                                <div class="form-group">
                                    <label for="exampleInputEmail1" class="exampleInputEmail1">Your Company
                                        Address</label>
                                    <textarea rows="4" cols="50" class="input-field" id="exampleInputEmail1"
                                              aria-describedby="emailHelp" name="company_address"
                                              placeholder="Enter company address">
                                        </textarea>

                                </div>
                            </div>
                            <div class="col-md-5 col-xs-12 col-sm-12">
                                <div class="form-group">
                                    <label for="exampleInputEmail1" class="exampleInputEmail1">Your Industry
                                        Detail</label>
                                    <textarea rows="4" cols="50" class="input-field" name="industry_detail"
                                              placeholder="Enter your Industry Detail">
                                        </textarea>

                                </div>
                            </div>
                            <div class="col-md-1 col-lg-1">
                            </div>
                            <div class="col-md-1 col-lg-1">
                            </div>
                        </div>
                        <div class="m-p-0 col-md-12 col-lg-12 col-xs-12 col-sm-12">

                            <div class="col-md-5 col-xs-12 col-sm-12">
                                <div class="chose">
                                    <p>Please provide your company logo in jpg, png or any other format.</p>
                                </div>
                                <div class="avatar-upload">

                                    <div class="avatar-edit">

                                        <input type="file" name="logo"><br><br>


                                    </div>
                                </div>
                            </div>
                            <div class="col-md-5 col-xs-12 col-sm-12">

                                <label class="container1 ">*I agree all statements in Terms of service
                                    <input type="checkbox"  required>
                                    <span class="checkmark" ></span>
                                </label>
                            </div>
                            <div class="col-md-1 col-lg-1">
                            </div>
                            <div class="col-md-1 col-lg-1">
                            </div>
                        </div>


                        <div class="text-right p-r-10 ">
                            <button class="btn-sub " type="submit">Submit</button>
                        </div>

                    </div>
                </form>
            </div>
        </div>
    </div>
</div>


<!-- Bootstrap js -->
<script src="{{ asset('./jquery/dist/jquery.js') }}"></script>

<script>



    @if (session('status'))

    alertify.set('notifier', 'position', 'top-right');
    alertify.success("{{ session('status') }}");


    @endif
</script>
<style>
    .alertify-notifier .ajs-message.ajs-error{
        color: #fff;
    }
</style>
<script>
    $('.cont').click(function () {

        var name = document.getElementById('name').value;
        var email = document.getElementById('email').value;

        if (name == '') {
            alertify.set('notifier', 'position', 'top-right');
            alertify.error("Name is required");
        }

        if (email == '') {
            alertify.set('notifier', 'position', 'top-right');
            alertify.error("Email is required");
        } else {
            var alphaExp = /^[a-zA-Z]+$/;
            var mailformat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;

            if (!name.match(alphaExp)) {

                alertify.set('notifier', 'position', 'top-right');
                alertify.error("Name must be string");
            }
            if (!email.match(mailformat)) {
                alertify.set('notifier', 'position', 'top-right');
                alertify.error("Please enter valid email address");
            } else {
                $("#home").hide();
                var nextId = $(this).parents('.tab-pane').next().attr("id");
                $('[href="#' + nextId + '"]').tab('show');

            }

        }


    });


</script>

<!-- Latest compiled and minified JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"
        integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous">
</script>


</body>

</html>
