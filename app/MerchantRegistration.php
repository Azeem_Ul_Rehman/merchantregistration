<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Storage;

class MerchantRegistration extends Model
{

    use SoftDeletes;
    protected $table = 'merchant_registrations';
    protected $fillable = [
        'name',
        'email',
        'company_name',
        'business_name',
        'company_registration',
        'contact_information',
        'company_address',
        'company_address',
        'industry_detail',
        'logo'
    ];


    public function setLogoAttribute($value)
    {
        $attribute_name = "logo";
        $disk = "public";
        $destination_path = "uploads/merchant/logo";
        if ($value == null) {
            Storage::disk($disk)->delete($this->{$attribute_name});
            $this->attributes[$attribute_name] = null;
        }

        if (isset($value)) {
            Storage::disk($disk)->delete($this->{$attribute_name});
            $filename = md5($value . time()) . '.png';
            $this->attributes[$attribute_name] = $value->store($destination_path, $disk);
        }
    }


    public static function boot()
    {
        parent::boot();
        static::deleting(function ($obj) {
            Storage::disk('public')->delete($obj->logo);
        });
    }
}
