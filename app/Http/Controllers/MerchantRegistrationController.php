<?php

namespace App\Http\Controllers;

use App\MerchantRegistration;
use Illuminate\Http\Request;

class MerchantRegistrationController extends Controller
{
    public function index(Request $request)
    {
        $registraion = MerchantRegistration::create([
            'name'=>$request->get('name'),
            'email'=>$request->get('email'),
            'company_name'=>$request->get('company_name'),
            'business_name'=>$request->get('business_name'),
            'company_registration'=>$request->get('company_registration'),
            'contact_information'=>$request->get('contact_information'),
            'company_address'=>$request->get('company_address'),
            'industry_detail'=>$request->get('industry_detail'),
            'logo'=>$request->file('logo')
        ]);


        return redirect('/')->with('status', 'Merchant Registered Successfully.');
    }
}
